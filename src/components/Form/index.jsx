import React from "react";
import Inputs from "../Inputs"

import "./styles.css";

export default function Form(props) {
  return (
    <div className="form">
      <Inputs registered={props.registered} />
    </div>
  )
}