import React, { useState } from "react";
import Title from "../Title"

import "./styles.css";

export default function Inputs(props) {

  const [title, setTitle] = useState("login");
  const [name, setName] = useState("");
  const [subname, setSubname] = useState("");

  const [contact, setContat] = useState({
      fName: "",
      lName: "",
      email: "",
  })

  function changeColor() {

    var color = document.getElementById("form-button").style.backgroundColor;

    if (color === "rgb(0, 0, 0)") {
      document.getElementById("form-button").style.backgroundColor = "#fff";
    } else {
      document.getElementById("form-button").style.backgroundColor = "#000";
    }
  }

  function handleChange(e) {

    const { name, value } = e.target;

    setContat(() => { return {...contact, [name]: value,} })

    setName(e.target.value);
  }


  function submitForm() {
    setTitle(name);
    setName("");
  }

  return (
    <>
      <Title text={`${title} ${contact.fName} ${contact.lName}`} />
      <p>{contact.email}</p>
      <input 
        name={"fName"} 
        type={"text"} 
        placeholder={"First Name"}
        onChange={handleChange}
        value={contact.fName}
      />
      <input 
        name={"lName"} 
        type={"text"} 
        placeholder={"Last Name"} 
        onChange={handleChange}
        value={contact.lName}
      />
      {
        props.registered ? (
          <input 
            name={"email"} 
            type={"email"} 
            placeholder={"E-mail"} 
            onChange={handleChange}
            value={contact.email}
          />
        ) : (
          null
        )
      }
      <button 
        id={"form-button"} 
        onMouseOver={() => changeColor()} 
        onMouseLeave={() => changeColor()}
        onClick={() => submitForm()}
      >
        Submit
      </button>
    </>
  )
}