import React from "react";

import "./styles.css";

export default function Title(props) {
  return (
  <h1>{props.text}</h1>
  )
}