import React, { useState  } from 'react';
import './App.css';

import Form from "./components/Form"
import Title from "./components/Title"

function App() {

  const [isLogged, setIsLogged] = useState(false);
  const [isRegistered] = useState(false);

  function hideShowForm() {
    if(isLogged) {
      setIsLogged(false);
    } else {
      setIsLogged(true);
    }
  }

  return (
    <div className="login-page">
      <input name={"checkbox-input"} type={"checkbox"} className={"magic-checkbox"} onChange={() => hideShowForm()} />
      {
        isLogged ? (
          <Title text={"Hello User!"} />
        ) : (
          isRegistered ? (
            <Form/>
          ) : (
            <Form registered={true}/>
          )
        )
      }
    </div>
  );
}

export default App;
